package br.com.qpainformatica.pdvserver;

import java.sql.Connection;
import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@Path("/compra")
public class CompraRest {
	
	@POST
	 @Path("/cadastro")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createCompra(Compra compra)
			throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		MysqlConnect db = new MysqlConnect();
		Connection conn = db.getConnection();
		CarrinhoDao compraDao = new CarrinhoDao();
		ItemDao itemDao = new ItemDao();
		
		compraDao.create(conn, compra.getCarrinho());
		
		for(Item item: compra.getItens()){
			itemDao.create(conn, item);
		}
		conn.close();

		return Response.status(Status.OK).build();
	}
	


		@GET
		 @Path("/todos")
		 @Produces({ MediaType.APPLICATION_JSON })
		public Response getAllProdutos()
				throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
				MysqlConnect db = new MysqlConnect();
				Connection conn = db.getConnection();
				CarrinhoDao carrinhoDao = new CarrinhoDao();
				
				TodasCompras todas = null;
				try {
					todas = carrinhoDao.getAllCompras(conn);
				} catch (NotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				conn.close();
			
			return Response.ok(todas).build();
		}

}
