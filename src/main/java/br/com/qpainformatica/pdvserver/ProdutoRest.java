package br.com.qpainformatica.pdvserver;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;




@Path("/produto")
public class ProdutoRest {
	
	@GET
	 @Path("/todos")
	 @Produces({ MediaType.APPLICATION_JSON })
	public Response getAllProdutos()
			throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
			MysqlConnect db = new MysqlConnect();
			Connection conn = db.getConnection();
			ProdutoDao produtoDao = new ProdutoDao();
			List<Produto> lista = produtoDao.loadAll(conn);
			conn.close();
		
		return Response.ok(lista).build();
	}



	@PUT
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response saveProduto(@FormParam("id") String id, @FormParam("descricao") String descricao,@FormParam("unidade") String unidade,@FormParam("preco") double preco, @FormParam("foto") String foto,@FormParam("ativo") int ativo,@FormParam("latitude") double latitude,@FormParam("longitude") double longitude)
			throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		MysqlConnect db = new MysqlConnect();
		Connection conn = db.getConnection();
		ProdutoDao produtoDao = new ProdutoDao();
		Produto produto = new Produto(id,descricao,unidade,preco,id,foto,latitude,longitude,ativo);
		try {
			produtoDao.save(conn, produto);
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Status.NOT_FOUND).build();
		}finally{
			conn.close();
		}
		return Response.status(Status.OK).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response createProduto(@FormParam("id") String id, @FormParam("descricao") String descricao,@FormParam("unidade") String unidade,@FormParam("preco") double preco, @FormParam("foto") String foto,@FormParam("ativo") int ativo,@FormParam("latitude") double latitude,@FormParam("longitude") double longitude)
			throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		MysqlConnect db = new MysqlConnect();
		Connection conn = db.getConnection();
		ProdutoDao produtoDao = new ProdutoDao();
		Produto produto = new Produto(id,descricao,unidade,preco,id,foto,latitude,longitude,ativo);
		try{
		produtoDao.create(conn, produto);
		}catch(MySQLIntegrityConstraintViolationException e){
			return Response.status(Status.CONFLICT).build();
		}finally{
			conn.close();
		}
		return Response.status(Status.OK).build();
	}
	
	

	@DELETE
	public Response deleteProduto(@QueryParam("id") String id)
			throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		MysqlConnect db = new MysqlConnect();
		Connection conn = db.getConnection();
		ProdutoDao produtoDao = new ProdutoDao();
		Produto p = new Produto();
		p.setId(id);
		try {
			produtoDao.delete(conn, p);
		} catch (NotFoundException e) {
			return Response.status(Status.NOT_FOUND).build();
		}finally{
			conn.close();
		}
		
		return Response.status(Status.OK).build();
	}
	
}
