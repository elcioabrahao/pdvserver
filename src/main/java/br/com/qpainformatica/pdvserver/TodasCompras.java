package br.com.qpainformatica.pdvserver;

import java.io.Serializable;
import java.util.List;

public class TodasCompras implements Serializable {
	

	private double totalCompra;
	private List<ItemProduto> listaProdutos;
	
	
	public List<ItemProduto> getListaProdutos() {
		return listaProdutos;
	}
	public void setListaProdutos(List<ItemProduto> listaProdutos) {
		this.listaProdutos = listaProdutos;
	}
	public double getTotalCompra() {
		return totalCompra;
	}
	public void setTotalCompra(double totalCompra) {
		this.totalCompra = totalCompra;
	}
	
	

}
