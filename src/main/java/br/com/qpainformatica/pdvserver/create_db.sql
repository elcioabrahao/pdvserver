# SQL command to create the table: 
# Remember to correct VARCHAR column lengths to proper values 
# and add additional indexes for your own extensions.

# If you had prepaired CREATE TABLE SQL-statement before, 
# make sure that this automatically generated code is 
# compatible with your own code. If SQL code is incompatible,
# it is not possible to use these generated sources successfully.
# (Changing VARCHAR column lenghts will not break code.)

CREATE TABLE produto (
      id varchar(255) NOT NULL,
      descricao varchar(255),
      unidade varchar(255),
      preco double,
      codigo_barras varchar(255),
      foto varchar(50000),
      latitude double,
      longitude double,
      ativo bigint,
PRIMARY KEY(id),
INDEX produto_id_INDEX (id));

CREATE TABLE compra (
      id bigint AUTO_INCREMENT NOT NULL,
      id_compra varchar(255),
PRIMARY KEY(id),
INDEX compra_id_INDEX (id));

CREATE TABLE item (
      id bigint NOT NULL,
      id_compra varchar(255),
      id_produto bigint,
      quantidade bigint,
PRIMARY KEY(id),
INDEX item_id_INDEX (id));



